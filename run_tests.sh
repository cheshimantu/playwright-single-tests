rm -rf ./allure-results
echo "browser=chromium" | tee ./allure-results/environment.properties
echo "execution=local" | tee -a ./allure-results/environment.properties
echo "arch=arm64" | tee -a ./allure-results/environment.properties

npm  i
npm test
# npx playwright show-report
# allure serve